/*
 *  Common code for the demos.
 *
 *  Author: Mar Alguacil
 */


/*
 *  Sends data and displays the received synthesized image.
 */
function generateFakeImage(section) {
    if (section == "interactivedemo")
        document.getElementById('segmap').value = ctx.canvas.toDataURL("image/png", 1);

    const fd = new FormData(document.forms["form-data"]);

    if (!fd.get('file_segmap') || !fd.get('file_style') ||
        (!fd.get('file_segmap').name && section != "interactivedemo") ||
        !fd.get('file_style').name)
       document.getElementById('messages-'+section).innerHTML =
                 '<div class="alert alert-warning alert-dismissible">' +
                 '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
                 'Segmentation Map or Style Image Not Found. Upload the missing one or check both, and try again.' +
                 '</div>';
    else {
        document.getElementById('fake-img-'+section).innerHTML =
                '<img id="span-style-"'+section+' class="icon-loading" src="/static/images/loading.gif" alt="">';

        let xhr = new XMLHttpRequest({mozSystem: true});
        xhr.open('POST', 'http://localhost:5000/'+section, true);
        xhr.responseType = "blob";

        xhr.onreadystatechange = function () {
            if (xhr.readyState == XMLHttpRequest.DONE) {
                if(xhr.response.type == 'image/png') {
                    document.getElementById('fake-img-'+section).innerHTML =
                             '<img class="dim-img" src="' + URL.createObjectURL(xhr.response) + '">';

                    // document.getElementById('messages-'+section).innerHTML =
                         // '<div class="alert alert-success alert-dismissible">' +
                         // '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
                         // 'Image successfully created and displayed.' +
                         // '</div>';
                }
                else {
                    document.getElementById('fake-img-'+section).innerHTML =
                             '<span id="fake-img-"'+section+'><div class="icon-fake"></div></span>';

                    document.getElementById('messages-'+section).innerHTML =
                             '<div class="alert alert-danger alert-dismissible">' +
                             '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
                             'Internal Server Error.' +
                             '</div>';
                }
            }
        }

        xhr.onload = function() {};
        xhr.send(fd);
    }
};