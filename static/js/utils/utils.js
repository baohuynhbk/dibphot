/*
 *  Utils
 *
 *  Author: Mar Alguacil
 */

/*
 * Converts colours in Hex to RGB format
 * Credit: https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
 */
function hex2rgb(hex) {
    let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

var utils = {
    /*
     * Changes pixel colour.
     */
    setPixelColorXY: function (x, y, color, save=true) {
        const r = (x + y * img.width) * 4;
        if (x >= 0 && x < img.width && y >= 0 && y < img.height &&
            (!save || savePixel(r))){
            img.data[r] = color.r;
            img.data[r+1] = color.g;
            img.data[r+2] = color.b;
            img.data[r+3] = 255;
        }
    },

    setPixelColor: function(r, color, save=true) {
        if (r >= 0 && r < img.width*img.height*4 &&
            (!save || savePixel(r))){
            img.data[r] = color.r;
            img.data[r+1] = color.g;
            img.data[r+2] = color.b;
            img.data[r+3] = 255;
        }
    }
}

/*
 * Checks whether two colours are the same.
 */
function colorMatch(r, color) {
    if (r >= 0 && r < img.width*img.height*4){
        return img.data[r] == color.r &&
               img.data[r+1] == color.g &&
               img.data[r+2] == color.b;
    }
    return -1;
}
