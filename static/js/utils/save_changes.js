/*
 *  Changes
 *
 *  Author: Mar Alguacil
 */

/*
 * Stores pixel changes.
 */
 class Changes {
    constructor(max_size=50) {
        this.max_size = max_size;
        this.previous_changes = [];
        this.tmp_changes = [];
    }

    /*
     * Adds an item to the end of the previous_changes list.
     */
    push(dict) {
        // Clear tmp_changes
        if (this.isNotEmptyTmpChanges())
            this.tmp_changes = [];

        // Remove the first item if the previous_changes list is full
        if (this.previous_changes.length == this.max_size)
            this.previous_changes.splice(0, 1);

        this.previous_changes.push(dict);
    }

    /*
     * Removes the last item from the previous_changes list,
     * adds the removed item to the tmp_changes list and
     * returns it.
     */
    popPreviousChanges() {
        if (this.isNotEmptyPreviousChanges()) {
            let change = this.previous_changes.pop();
            this.tmp_changes.push(change);

            return change;
        }
        return {};
    }

    /*
     * Removes the last item from the tmp_changes list,
     * adds the removed item to the previous_changes list and
     * returns it.
     */
    popTmpChanges() {
        if (this.isNotEmptyTmpChanges()) {
            let change = this.tmp_changes.pop();
            this.previous_changes.push(change);

            return change;
        }
        return {};
    }

    /*
     * Get the length of the previous_changes list.
     */
    get sizePreviousChanges() {
        return this.previous_changes.length;
    }

    /*
     * Checks if the previous_changes list is not empty.
     */
    isNotEmptyPreviousChanges() {
        return this.previous_changes.length > 0;
    }

    /*
     * Get the length of the tmp_changes list.
     */
    get sizeTmpChanges() {
        return this.tmp_changes.length;
    }

    /*
     * Checks if the previous_changes list is not empty.
     */
    isNotEmptyTmpChanges() {
        return this.tmp_changes.length > 0;
    }

    /*
     * Delete all items from the previous_changes and tmp_changes list.
     */
    clear() {
        this.previous_changes = [];
        this.tmp_changes = [];
    }
}