/*
 *  Static Demo JS
 *
 *  Author: Mar Alguacil
 */

/* Opens filesystem. */
document.getElementById("add-segmap-staticdemo").onclick = function() {
    document.getElementById("upload-segmap-staticdemo").click();
};

document.getElementById("add-style-staticdemo").onclick = function() {
    document.getElementById("upload-style-staticdemo").click();
};


/* Previews image. */
document.getElementById("upload-segmap-staticdemo").onchange = function() {
    var file = this.files[0];

    if (['image/jpeg', 'image/png'].indexOf(file.type) != -1)
        document.getElementById("span-segmap-staticdemo").outerHTML =
                '<img id="span-segmap-staticdemo" class="dim-img" src="' + URL.createObjectURL(file) + '">';
    else
        document.getElementById("messages-staticdemo").innerHTML =
                 '<div class="alert alert-warning alert-dismissible">' +
                 '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
                 'Unsupported File Format. <strong>Supported Image File Formats:</strong> PNG(.png), JPEG(.jpg, .jpeg).' +
                 '</div>';

};

document.getElementById("upload-style-staticdemo").onchange = function() {
    var file = this.files[0];

    if (['image/jpeg', 'image/png'].indexOf(file.type) != -1)
        document.getElementById("span-style-staticdemo").outerHTML =
                '<img id="span-style-staticdemo" class="dim-styimg" src="' + URL.createObjectURL(file) + '">';
    else
        document.getElementById("messages-staticdemo").innerHTML =
                 '<div class="alert alert-warning alert-dismissible">' +
                 '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
                 'Unsupported File Format. <strong>Supported Image File Formats:</strong> PNG(.png), JPEG(.jpg, .jpeg).' +
                 '</div>';

};