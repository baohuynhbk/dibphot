/*
 *  Interactive Demo JS
 *
 *  Author: Mar Alguacil
 */

/* Opens filesystem. */
document.getElementById("add-style-interactivedemo").onclick = function() {
    document.getElementById("upload-style-interactivedemo").click();
};

/* Previews image. */
document.getElementById("upload-style-interactivedemo").onchange = function() {
    var file = this.files[0];

    if (['image/jpeg', 'image/png'].indexOf(file.type) != -1)
        document.getElementById("span-style-interactivedemo").outerHTML =
                '<img id="span-style-interactivedemo" class="dim-styimg" src="' + URL.createObjectURL(file) + '">';
    else
        document.getElementById("messages-interactivedemo").innerHTML =
                 '<div class="alert alert-warning alert-dismissible">' +
                 '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
                 'Unsupported File Format. <strong>Supported Image File Formats:</strong> PNG(.png), JPEG(.jpg, .jpeg).' +
                 '</div>';

};

/* Changes button style after clicking.
 * Credit: https://stackoverflow.com/questions/31178653/how-to-keep-active-css-style-after-click-a-button
 */
$(".tool").on("click", function(){
    $(".tool").removeClass("selected");
    $(this).addClass('selected');
});

$(".tool-bubble").on("click", function(){
    $(".tool-bubble").removeClass("selected");
    $(this).addClass('selected');
});

$(".circle-button").on("click", function(){
    $(".circle-button").removeClass("selected");
    $(this).addClass('selected');
});


/****************************************************
 *                                                  *
 *                      CANVAS                      *
 *                                                  *
 ****************************************************/

let ctx = document.getElementById('paint-segmap').getContext('2d'),
    changes = new Changes();

// The size of the canvas must match the size shown on the page,
// which is dynamically defined in "interative-demo.css".
ctx.canvas.width  = ctx.canvas.offsetWidth;
ctx.canvas.height = ctx.canvas.offsetHeight;
window.onresize = function() {
    // Make an in-memory canvas
    let tmp_canvas = document.createElement('canvas'),
        tmp_ctx = tmp_canvas.getContext('2d');
    // Save image
    tmp_canvas.width = ctx.canvas.width;
    tmp_canvas.height = ctx.canvas.height;
    tmp_ctx.drawImage(ctx.canvas, 0, 0);
    // Resize canvas
    ctx.canvas.width  = ctx.canvas.offsetWidth;
    ctx.canvas.height = ctx.canvas.offsetHeight;
    // Redraw the image back on canvas
    ctx.drawImage(tmp_canvas, 0, 0, tmp_canvas.width, tmp_canvas.height,
                              0, 0, ctx.canvas.width, ctx.canvas.height);
}

// Default configuration
ctx.strokeStyle = '#0F9601';
ctx.fillStyle = '#0F9601';
ctx.lineWidth = 1;
setPixelColorXY = utils.setPixelColorXY;
setPixelColor = utils.setPixelColor;
// Colour the canvas
clearCanvas();


/*---------------- Stroke width ----------------*/
function changeStrokeWidthRange(new_width) {
    document.getElementById("stroke-width-number").value = new_width;
    ctx.lineWidth = new_width;
}
function changeStrokeWidthNumber(new_width) {
    document.getElementById("stroke-width-range").value = new_width;
    ctx.lineWidth = new_width;
}


/**************************
 *          TOOLS         *
 **************************/

/*-------------------- Paintbrush --------------------*/
document.getElementById("paintbrush-tool").onclick = function () {
    let bubble_style = document.getElementById("speech-bubble").style,
        stroke_style = document.getElementById("stroke-width").style;

    // Deselect the button of the previously selected tool
    $(".tool-bubble").removeClass("selected");

    // Configure speech bubble
    bubble_style.setProperty('--border-left-width', "auto");
    bubble_style.setProperty('--margin-left', '-7vmax');

    // Display speech bubble
    bubble_style.display = "block";

    // Display change in stroke-width
    stroke_style.display = "flex";
    stroke_style.justifyContent = "space-evenly";

    // Change tooltip tittles
    document.getElementById("line").title = "Flat-tip brush"
    document.getElementById("rectangle").title = "Square-tip brush"
    document.getElementById("rhombus").title = "Diamond-tip brush"
    document.getElementById("circle").title = "Rounded-tip brush"

    // Configuration
    drawing = false;

    // Click
    const onmousedown = (e) => {
        pixels = [];
        previous_colors = [];

        // We obtain the corresponding coordinates on the canvas,
        // by knowing the point where the page has been clicked on and
        // the coordinates of the left-top corner of the canvas
        x = e.pageX - ctx.canvas.offsetLeft;
        y = e.pageY - ctx.canvas.offsetTop;

        drawing = true;
        pop = true;
    }
    // Move mouse
    const onmousemove = (e, plot_function) => {
        if (drawing) {
            const x_prev = x,
                  y_prev = y;
            x = e.pageX - ctx.canvas.offsetLeft;
            y = e.pageY - ctx.canvas.offsetTop;

            const change = plot_function(x_prev, y_prev, x, y);

            if (pop)
                change_prev = changes.popPreviousChanges();
            changes.push({
                pixels: change_prev.pixels.concat(change.pixels),
                previous_color: change_prev.previous_color.concat(change.previous_color),
                color: change.color
            });
        }
    }
    // Release mouse click
    const onmouseup = (e, plot_function) => {
        if (drawing) {
            const change = plot_function(x, y,
                                         e.pageX - ctx.canvas.offsetLeft, e.pageY - ctx.canvas.offsetTop);

            // Save the changed pixels, along with their previous colours and the new one
            change_prev = changes.popPreviousChanges();
            changes.push({
                pixels: change_prev.pixels.concat(change.pixels),
                previous_color: change_prev.previous_color.concat(change.previous_color),
                color: change.color
            });

            // Reset configuration
            drawing = false;
            pop = false;
        }
    }

    /* Flat-tip brush */
    document.getElementById("line").onclick = function () {
        ctx.canvas.onmousedown = function(e) {
            onmousedown(e);

            changes.push({
                pixels: [],
                previous_color: [],
                color: ''
            });
        };

        ctx.canvas.onmousemove = function(e) {
            onmousemove(e, plotLine);
        };

        ctx.canvas.onmouseup = function(e) {
            onmouseup(e, plotLine);
        };
    }

    /* Square-tip brush */
    document.getElementById("rectangle").onclick = function () {
        const plot_line_shape = (x_prev, y_prev, x_curr, y_curr) =>
            plotLineShape(x_prev, y_prev, x_curr, y_curr, brush_tip_shape);

        ctx.canvas.onmousedown = function(e) {
            onmousedown(e);

            // Square
            const plot_square = (x_, y_, half_size) => plotRectangle(x_-half_size, y_-half_size,
                                                                     ctx.lineWidth, ctx.lineWidth)
            brush_tip_shape = drawonmousedown(x, y, plot_square);

            changes.push({
                pixels: pixels,
                previous_color: previous_colors,
                color: hex2rgb(ctx.fillStyle)
            });
        };

        ctx.canvas.onmousemove = function(e) {
            onmousemove(e, plot_line_shape);
        };

        ctx.canvas.onmouseup = function(e) {
            onmouseup(e, plot_line_shape);
        };
    }

    /* Diamond-tip brush */
    document.getElementById("rhombus").onclick = function () {
        const plot_line_shape = (x_prev, y_prev, x_curr, y_curr) =>
            plotLineShape(x_prev, y_prev, x_curr, y_curr, brush_tip_shape);

        ctx.canvas.onmousedown = function(e) {
            onmousedown(e);

            // Rhombus
            const plot_rhombus = (x_, y_, half_size) => plotRhombus(x_, y_, // center
                                                                    x_, y_-half_size); // bottom corner
            brush_tip_shape = drawonmousedown(x, y, plot_rhombus);

            changes.push({
                pixels: pixels,
                previous_color: previous_colors,
                color: hex2rgb(ctx.fillStyle)
            });
        };

        ctx.canvas.onmousemove = function(e) {
            onmousemove(e, plot_line_shape);
        };

        ctx.canvas.onmouseup = function(e) {
            onmouseup(e, plot_line_shape);
        };
    }


    /* Rounded-tip brush */
    document.getElementById("circle").onclick = function () {
        const plot_line_shape = (x_prev, y_prev, x_curr, y_curr) =>
            plotLineShape(x_prev, y_prev, x_curr, y_curr, brush_tip_shape);

        ctx.canvas.onmousedown = function(e) {
            onmousedown(e);

            // Circle
            brush_tip_shape = drawonmousedown(x, y, plotCircle);

            changes.push({
                pixels: pixels,
                previous_color: previous_colors,
                color: hex2rgb(ctx.fillStyle)
            });
        };

        ctx.canvas.onmousemove = function(e) {
            onmousemove(e, plot_line_shape);
        };

        ctx.canvas.onmouseup = function(e) {
            onmouseup(e, plot_line_shape);

        };
    }

    // Click on Rounded-tip Brush by default
    document.getElementById("circle").click();
}


/*-------------------- Geometric shapes --------------------*/
document.getElementById("geometric-shape-tool").onclick = function () {
    let bubble_style = document.getElementById("speech-bubble").style,
        stroke_style = document.getElementById("stroke-width").style;

    // Deselect the button of the previously selected tool
    $(".tool-bubble").removeClass("selected");

    // Configure speech bubble
    bubble_style.setProperty('--border-left-width', "1vmax");
    bubble_style.setProperty('--margin-left', "-3.5vmax");

    // Display speech bubble
    bubble_style.display = "block";

    // Hide change in stroke-width
    stroke_style.display = "none";

    // Change tooltip tittles
    document.getElementById("line").title = "Line"
    document.getElementById("rectangle").title = "Rectangle"
    document.getElementById("rhombus").title = "Rhombus"
    document.getElementById("circle").title = "Circle"

    // Configuration
    drawing = false;

    // Click
    const onmousedown = (e) => {
        prev_img = ctx.getImageData(0, 0, ctx.canvas.width, ctx.canvas.height);

        // We obtain the corresponding coordinates on the canvas,
        // by knowing the point where the page has been clicked on and
        // the coordinates of the left-top corner of the canvas
        x = e.pageX - ctx.canvas.offsetLeft;
        y = e.pageY - ctx.canvas.offsetTop;

        drawing = true;
    }
    // Move mouse
    const onmousemove = (e, plot_function) => {
        if (drawing) {
            ctx.putImageData(prev_img, 0, 0);

            // Draw a temporary geometric shape
            plot_function(x, y,
                          e.pageX - ctx.canvas.offsetLeft, e.pageY - ctx.canvas.offsetTop);
        }
    }
    // Release mouse click
    const onmouseup = (e, plot_function) => {
        if (drawing) {
            ctx.putImageData(prev_img, 0, 0);

            // Draw the definitive geometric shape
            const change = plot_function(x, y,
                                         e.pageX - ctx.canvas.offsetLeft, e.pageY - ctx.canvas.offsetTop);

            // Save the changed pixels, along with their previous colours and the new one
            changes.push(change);

            // Reset configuration
            drawing = false;
        }
    }

    /* Draw a line */
    document.getElementById("line").onclick = function () {
        // Display change in stroke-width
        stroke_style.display = "flex";
        stroke_style.justifyContent = "space-evenly";

        ctx.canvas.onmousedown = function(e) {
            onmousedown(e);
        };

        ctx.canvas.onmousemove = function(e) {
            onmousemove(e, plotLine);
        };

        ctx.canvas.onmouseup = function(e) {
            onmouseup(e, plotLine);
        };
    }

    /* Draw a rectangle */
    document.getElementById("rectangle").onclick = function () {
        // Hide stroke-width
        stroke_style.display = "none";

        // Function for drawing a rectangle from two opposite corner points
        const plot_rectangle = (x_prev, y_prev, x_curr, y_curr) =>
            plotRectangle(x_prev, y_prev, x_curr - x, y_curr - y);

        ctx.canvas.onmousedown = function(e) {
            onmousedown(e);
        };

        ctx.canvas.onmousemove = function(e) {
            onmousemove(e, plot_rectangle);
        };

        ctx.canvas.onmouseup = function(e) {
            onmouseup(e, plot_rectangle);
        };
    }

    /* Draw a rhombus */
    document.getElementById("rhombus").onclick = function () {
        // Hide stroke-width
        stroke_style.display = "none";

        // Function for drawing a rhombus from two opposite corner points
        const plot_rhombus = (x_prev, y_prev, x_curr, y_curr) =>
            plotRhombus(Math.round((x_prev+x_curr)/2), Math.round((y_prev+y_curr)/2), // center
                        x_prev, y_prev); // a corner

        ctx.canvas.onmousedown = function(e) {
            onmousedown(e);
        };

        ctx.canvas.onmousemove = function(e) {
            onmousemove(e, plot_rhombus);
        };

        ctx.canvas.onmouseup = function(e) {
            onmouseup(e, plot_rhombus);
        };
    }

    /* Draw a circle */
    document.getElementById("circle").onclick = function () {
        // Hide stroke-width
        stroke_style.display = "none";

        // Function for drawing a circle from two opposite points
        const plot_circle = (x_prev, y_prev, x_curr, y_curr) =>
            plotCircle(Math.round((x_prev+x_curr)/2), Math.round((y_prev+y_curr)/2), // center
                       Math.round(Math.hypot(x_curr-x_prev, y_curr-y_prev)/2)); // radius

        ctx.canvas.onmousedown = function(e) {
            onmousedown(e);
        };

        ctx.canvas.onmousemove = function(e) {
            onmousemove(e, plot_circle);
        };

        ctx.canvas.onmouseup = function(e) {
            onmouseup(e, plot_circle);
        };
    }

    // Click on Circle by default
    document.getElementById("circle").click();
}


/*-------------------- Bucket --------------------*/
document.getElementById("bucket-fill-tool").onclick = function () {
    let bubble_style = document.getElementById("speech-bubble").style;

    // Hide speech bubble
    bubble_style.display = "None";

    ctx.canvas.onmousedown = function(e) {
        // Fill selected area with a colour
        const change = fill(e.pageX - ctx.canvas.offsetLeft, e.pageY - ctx.canvas.offsetTop)

        // Save the changed pixels, along with their previous colours and the new one
        changes.push(change);
    }
}


/*-------------------- Eyedropper --------------------*/
document.getElementById("eyedropper-tool").onclick = function () {
    let bubble_style = document.getElementById("speech-bubble").style;

    // Hide speech bubble
    bubble_style.display = "None";

    ctx.canvas.onmousedown = function(e) {
        new_color = ctx.getImageData(e.pageX - ctx.canvas.offsetLeft, e.pageY - ctx.canvas.offsetTop,
                                     1, 1).data;
        changeColor(`rgba(${new_color[0]}, ${new_color[1]}, ${new_color[2]}, 255)`);
    }
}



/****************************
 *          COLOURS         *
 ****************************/
function changeColor(new_color) {
    ctx.strokeStyle = new_color;
    ctx.fillStyle = new_color;

    document.getElementById("color-selected").style.background = ctx.strokeStyle;
}


/********************************
 *          UNDO / REDO         *
 ********************************/
 /*
  * Undos changes by extracting items from the previous_changes list.
  */
function undo() {
    drawing = false; pop = false;
    if (changes.isNotEmptyPreviousChanges()) {
        img = ctx.getImageData(0, 0, ctx.canvas.width, ctx.canvas.height);
        const change = changes.popPreviousChanges();

        for (let i = change.pixels.length-1; i>=0; --i)
            setPixelColor(change.pixels[i], change.previous_color[i], save=false);

        // Update canvas
        ctx.putImageData(img, 0, 0);
    }
}

 /*
  * Redos changes by extracting items from the tmp_changes list.
  */
function redo() {
    if (changes.isNotEmptyTmpChanges()) {
        img = ctx.getImageData(0, 0, ctx.canvas.width, ctx.canvas.height);
        const change = changes.popTmpChanges(),
              color = hex2rgb(change.color);

        for (r of change.pixels)
            setPixelColor(r, color, save=false);

        // Update canvas
        ctx.putImageData(img, 0, 0);
    }
}



/*********************************
 *          CLEAR CANVAS         *
 *********************************/
/*
 * Resets canvas colour.
 */
function clearCanvas() {
    img = ctx.getImageData(0, 0, ctx.canvas.width, ctx.canvas.height);
    const sky_color = hex2rgb('#69DAE9'),
          sea_color = hex2rgb('#44A2A2');

    // Sky
    for (let i=0; i<ctx.canvas.width; ++i)
        for (let j=0; j<Math.round(ctx.canvas.height/2); ++j)
            setPixelColorXY(i, j, sky_color, save=false);

    // Sea
    for (let i=0; i<ctx.canvas.width; ++i)
        for (let j=Math.round(ctx.canvas.height/2); j<ctx.canvas.height; ++j)
            setPixelColorXY(i, j, sea_color, save=false);

    // Update canvas
    ctx.putImageData(img, 0, 0);


    // Delete previous changes
    changes.clear();
}

/*************************
 *          INIT         *
 *************************/
function init() {
    document.getElementById("paintbrush-tool").click();
    document.getElementById("sky").click();
}
