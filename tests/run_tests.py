"""
    Run all the tests.

    Author: Mar Alguacil
"""
import unittest
from test_home_about import TestHomeAbout
from test_static_demo import TestStaticDemo
from test_interactive_demo import TestInteractiveDemo


if __name__ == '__main__':
    unittest.main()

