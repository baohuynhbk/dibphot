"""
    Basis for the Test Classes.

    Author: Mar Alguacil
"""
import unittest
import warnings
# Change directory to import app
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import app

class TestBasic(unittest.TestCase):
    def setUp(self):
        warnings.simplefilter("ignore", ResourceWarning)
        self.app = app.app.test_client()

    def check(self, values, expected_values, section, text_info):
        """Checks status code and displays info clearly.

           If the status code was as expected, it shows a check mark and the
          info in green. Otherwise, it shows a cross mark and the info in red.
        """
        try:
            if isinstance(values, list):
                for value, expected_value in zip(values, expected_values):
                    self.assertEqual(value, expected_value)
            else:
                self.assertEqual(values, expected_values)
            # If everything is OK, display a check mark and the info in green
            print('\033[0;32m \u2714 \033[1;32m ['+ section +
                  '] \033[0;32m ' + text_info + '\033[0m')
        except AssertionError:
            # If something is wrong, display a cross mark and the info in red
            print('\033[0;31m \u2718 \033[1;31m ['+ section +
                  '] \033[0;31m ' + text_info + '\033[0m')
            raise

