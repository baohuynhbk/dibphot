"""
    Static Demo Tests.

    Author: Mar Alguacil
"""
import io
from basic import TestBasic

class TestStaticDemo(TestBasic):
    def __init__(self, *args, **kwargs):
        super(TestStaticDemo, self).__init__(*args, **kwargs)
        file_segmap = "./tests/images/ADE_train_00003129.png"
        file_style = "./tests/images/ADE_train_00003129.jpg"

        self.opened_file_segmap = (open(file_segmap, 'rb'), file_segmap)
        self.opened_file_style = (open(file_style, 'rb'), file_style)

    def test_get(self):
        """Tests Static Demo view display."""
        result = self.app.get('/staticdemo')
        self.check(result.status_code, 200,
                   'Static Demo', "Page successfully rendered.")

    def test_post_generate_fake_image(self):
        """Checks if the photorealistic image is correctly created from a segmentation map
          and a style image, and returned.
        """
        data = {
            'file_segmap': self.opened_file_segmap,
            'file_style': self.opened_file_style
        }
        result = self.app.post('/staticdemo', data=data)
        self.check(result.status_code, 200,
                   'Static Demo', "Synthesized image successfully created.")
        self.check(result.content_type, 'image/PNG',
                   'Static Demo', "Synthesized image successfully returned.")

    def test_post_no_files(self):
        """Checks that the page is redirected after detecting that at least one file is missing."""
        result = self.app.post('/staticdemo')
        self.check(result.status_code, 302,
                   'Static Demo', "No files.")

        data = {
            'file_style': self.opened_file_style
        }
        result = self.app.post('/staticdemo', data=data)
        self.check([result.status_code, result.content_type], [302, 'text/html; charset=utf-8'],
                   'Static Demo', "No segmentation map.")

        data = {
            'file_segmap': self.opened_file_segmap
        }
        result = self.app.post('/staticdemo', data=data)
        self.check([result.status_code, result.content_type], [302, 'text/html; charset=utf-8'],
                   'Static Demo', "No style image.")


    def test_post_empty_files(self):
        """Checks that the page is redirected after detecting that at least one file is empty."""
        data = {
            'file_segmap': (io.BytesIO(b''), ''),
            'file_style': self.opened_file_style
        }
        result = self.app.post('/staticdemo', data=data)
        self.check([result.status_code, result.content_type], [302, 'text/html; charset=utf-8'],
                   'Static Demo', "Empty segmentation map.")

        data = {
            'file_segmap': self.opened_file_segmap,
            'file_style': (io.BytesIO(b''), '')
        }
        result = self.app.post('/staticdemo', data=data)
        self.check([result.status_code, result.content_type], [302, 'text/html; charset=utf-8'],
                   'Static Demo', "Empty style image.")

    def test_post_unsupported_format(self):
        """Checks that the page is redirected after detecting that at least one file has an
          unsupported format.
        """
        data = {
            'file_segmap': (io.BytesIO(b'Some text data.'), 'unsupported_file.txt'),
            'file_style': self.opened_file_style
        }
        result = self.app.post('/staticdemo', data=data)
        self.check([result.status_code, result.content_type], [302, 'text/html; charset=utf-8'],
                   'Static Demo', "Unsupported segmentation map file format.")

        data = {
            'file_segmap': self.opened_file_segmap,
            'file_style': (io.BytesIO(b'Some text data.'), 'unsupported_file.txt')
        }
        result = self.app.post('/staticdemo', data=data)
        self.check([result.status_code, result.content_type], [302, 'text/html; charset=utf-8'],
                   'Static Demo', "Unsupported style image file format.")


