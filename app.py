"""
    Semantic Image Synthesis (website).

    Author: Mar Alguacil
"""
from flask import  Flask, request, flash, render_template, redirect, send_file
from werkzeug.utils import secure_filename
from utils.web.demos import *
from utils.web.interactive_demo import *
import os


#################
#   Flask app   #
#################
app = Flask(__name__)
app.secret_key = os.environ.get('SECRET_KEY')


#################
#   EndPoints   #
#################
@app.route("/")
def home():
    return render_template("home.html")


@app.route("/staticdemo", methods=['GET', 'POST'])
def staticDemo():
    if request.method == "POST":
        if 'file_segmap' not in request.files or 'file_style' not in request.files:
            flash('No segmentation map or style image.')
            return redirect(request.url)

        f_segmap = request.files['file_segmap']
        f_style = request.files['file_style']
        if f_segmap.filename == '' or f_style.filename == '':
            flash('Empty segmentation map or style image.')
            return redirect(request.url)


        _, file_extension_segmap = os.path.splitext(secure_filename(f_segmap.filename))
        _, file_extension_style = os.path.splitext(secure_filename(f_style.filename))

        if file_extension_segmap.lower() in [".jpeg", ".jpg", ".png"] and \
           file_extension_style.lower() in [".jpeg", ".jpg", ".png"]:
            segmap = file2img(f_segmap, color_mode=cv2.IMREAD_GRAYSCALE)
            styimg = file2img(f_style)

            return send_file(generateFake(segmap, styimg), mimetype='image/PNG')

        flash('Unsupported File Format. Supported Image File Formats: PNG(.png), JPEG(.jpg, .jpeg).')
        return redirect(request.url)

    return render_template("static-demo.html")


@app.route("/interactivedemo", methods=['GET', 'POST'])
def interactiveDemo():
    if request.method == "POST":
        if 'file_segmap' not in request.form or 'file_style' not in request.files:
            flash('No segmentation map or style image.')
            return redirect(request.url)

        f_style = request.files['file_style']
        if f_style.filename == '':
            flash('Empty style image.')
            return redirect(request.url)

        _, file_extension_style = os.path.splitext(secure_filename(f_style.filename))
        if file_extension_style.lower() in [".jpeg", ".jpg", ".png"]:
            # STYLE IMAGE
            styimg = file2img(f_style)

            # SEGMENTATION MAP
            # Read the segmentation map from a base64 data URL
            b64_string = request.form['file_segmap']
            # Convert the data URL to an OpenCV image
            segmap = uri2img(b64_string)
            # Transform the color segmentation map to id segmentation map
            segmap = transformSegmentationMapFromColor(segmap)

            # SYNTHESIZED IMAGE
            return send_file(generateFake(segmap, styimg), mimetype='image/PNG')

        flash('Unsupported File Format. Supported Image File Formats: PNG(.png), JPEG(.jpg, .jpeg).')
        return redirect(request.url)

    return render_template("interactive-demo.html")


@app.route("/about")
def about():
    return render_template("about.html")


###############
#   Run app   #
###############
if __name__ == '__main__':
    app.run(debug=True)
