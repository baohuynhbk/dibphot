"""
   Auxiliary web functions for interative demo.

   Author: Mar Alguacil
"""
import cv2
import base64
import numpy as np
from ast import literal_eval

def uri2img(b64_string):
    """Converts a data URL into an OpenCV image.
    Credit: https://stackoverflow.com/a/54205640/2415512
    """
    encoded_data = b64_string.split(',')[1]
    nparr = np.frombuffer(base64.b64decode(encoded_data), np.uint8)

    img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)

    # return cv2.imdecode(nparr, cv2.IMREAD_GRAYSCALE)
    return cv2.cvtColor(img, cv2.COLOR_BGR2RGB)


with open('./labels/color_semantic_labels.txt', 'r') as f:
    color_labels = literal_eval(f.read())
def transformSegmentationMapFromColor (segmap_color):
    """Transforms the given color segmentation map into a grayscale segmentation map
      by assigning the corresponding identifier to each color.
    """
    segmap = np.zeros((segmap_color.shape[0], segmap_color.shape[1]), dtype=np.uint8)

    for color, id in color_labels.items():
        color_array = np.asarray(color, np.float32).reshape([1, 1, -1])
        m = np.all(segmap_color == color_array, axis=-1)
        segmap[m] = id

    return segmap